#!/bin/bash
# Elogind script for open-fprintd resume and suspend
# Copy this script into /lib/lib{,64}/elogind/system-sleep
# Written by T. M. Ghazi Al-Hizmi <alhizmei@gmail.com>, 2022

case $1 in
	pre/*)
		/usr/lib/open-fprintd/suspend.py
		;;
	post/*)
		/usr/lib/open-fprintd/resume.py
		;;
esac
